# Ejercicios propuestos - Casa

1. Crear una matriz de 7x7 y rotarla 90 grados en sentido horario.
2. Crear una matriz de 7x7 y llenarla con números aleatorios del 1 al 100.
3. Crear una matriz de 5x5 y multiplicarla por otra matriz de 3x3.
4. Crear una matriz de 7x7 y encontrar la forma canónica de Jordan.
5. Crear un arreglo de enteros y encontrar el subarreglo de longitud máxima en orden creciente.
6. Crear un arreglo de cadenas y ordenarlo en orden lexicográfico inverso.
7. Crear un arreglo de enteros y encontrar el k-ésimo elemento más grande.
8. Crear un arreglo de enteros y encontrar el número de parejas de elementos cuya suma es un número primo.
9. Crear un arreglo de enteros y encontrar la mediana.
10. Crear un arreglo de enteros y encontrar la moda.
11. Crear un arreglo de enteros y encontrar la media.
12. Crear un programa que encuentre el número más grande de un arreglo utilizando un bucle
13. Crear un programa que cuente cuántos números impares hay en un arreglo utilizando un bucle.
14. Crear un programa que cuente cuántas veces aparece un número específico en un arreglo utilizando un algoritmo de búsqueda binaria.
15. Crear un programa que cuente cuántos números pares hay en un arreglo utilizando un bucle.
16. Encontrar el número que se repite más veces en un array.

17. Dividir un array en dos partes iguales.
18. Calcular el producto punto entre dos arrays.
19. Crear un programa que determine si una letra es una vocal o una consonante.
20. Crear un programa que permita crear un arreglo de todos los códigos ASCII.