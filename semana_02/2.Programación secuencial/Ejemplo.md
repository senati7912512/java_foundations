# Primera forma

```java
/**

* ejercicio_01
* Crear un método que calcule la suma de los elementos de un arreglo de enteros
* Autor: Franklin C.
 */
import java.util.Scanner;

class ejemplo_01{

    void lectura_teclado(){
        
    }

    int suma_numeros(int n){
        int[] numero = new int[n];
        int suma = 0;
        Scanner leer_numero = new Scanner(System.in);

        for(int i=0 ; i < n; i++ ){
            numero[i] = leer_numero.nextInt();
            suma = suma + numero[i];
        }
        return suma;
    }

    public static void main(String[] args){
        Scanner leer_numero = new Scanner(System.in);

        int n;

        System.out.println("¿Cuántos números ingresara?: ");
        n = leer_numero.nextInt();

        ejemplo_01 suma_numeros = new ejemplo_01();

        System.out.println("La suma de números es: " + suma_numeros.suma_numeros(n));
    }

}
```

# Segunda forma

```java
/**

* ejercicio_01
* Crear un método que calcule la suma de los elementos de un arreglo de enteros
* Autor: Franklin C.
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

 public class ejercicio_01 {
    private static BufferedReader lectura_teclado = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String[] args) throws NumberFormatException, IOException {
        int cantidad_datos;
        System.out.print("¿Cuántos números ingresara?: ");
        cantidad_datos = leerEntero(lectura_teclado);
        
        //Ingresamos los valores del array llamando al método crear_array
        int datos[] = crear_array(cantidad_datos);

        //Imprimimos el array mediante el método imprimir_array
        imprimir_array(datos);

        //Usamos al método suma_array
        System.out.print("\nLa suma del arraym es: " + suma_array(datos));
    }

    public static int[] crear_array(int longitud) throws NumberFormatException, IOException {
        int array[] = new int[longitud];
        for (int i = 0; i < array.length; i++) {
            System.out.print("dato[" + i + "]: ");
            array[i] = leerEntero(lectura_teclado);
        }
        return array;
    }

    public static void imprimir_array(int[] array) {
        System.out.println("\nEl array: ");
        for (int i : array ) {
            System.out.print(i+ " ");
        }
    }

    public static int suma_array(int[] array) {
        int suma = 0;
        for (int i = 0; i < array.length; i++) {
            suma += array[i];
        }
        return suma;
    }

    public static int leerEntero(BufferedReader lectura_teclado) throws IOException, NumberFormatException {
        String entrada = lectura_teclado.readLine();
        return Integer.parseInt(entrada);
    }

}
```

# Tercera forma


