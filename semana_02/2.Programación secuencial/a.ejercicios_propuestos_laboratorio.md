# Ejercicios propuestos - Laboratorio

> Nota: Todos los programas a resolver, deben emplear métodos

1. Crear un método que calcule la suma de los elementos de un arreglo de enteros.
2. Crear un método que calcule el producto de los elementos de un arreglo de enteros.
3. Desarrollar un programa que permita almacenar los datos de los estudiantes de Ing. del Software de SENATI en un areglo bidimencional, donde los datos son nombres y edad, el programa deberá calcular la edad promedio de los estuadiantes, el programa de hacer uso de métodos.
4. Crear un método que encuentre el elemento máximo en un arreglo de enteros.
5. Empleando Switch, y métodos crear un programa que permita calcular el area y perímetro de las siguientes figuras geométricas, cuadrado, rombo, rectángulo y circunferencia, adicionalmente el programana debe mermitir dibujar las figuras geométricas empleando "*".
6. Crear un programa que permita crear el factorial de "N" números grandes (ejemplo 50) menores o iguales que 100; todos estos resultados deben ser almacenados en un array, finalmente el programa debe permitir imprimirlos en pantalla.
```sh
¿Cuántos números ingresará?: 3
Salida:
El factorial de 5 es: 120
El factorial de 10 es: 3628800
El factorial de 10 es: 93326215443944152681699238856266700490715968264381621468592963895217599993229915608941463976156518286253697920827223758251185210916864000000000000000000000000
```
7. Crear un método que busque un elemento en un arreglo de enteros y devuelva su posición.
8. Crear un método que elimine los elementos duplicados de un arreglo de enteros.
9. Crear un método que calcule la varianza de un arreglo de enteros.
10. Crear un método que ordene un arreglo de enteros de manera descendente.
11. Crea un array de números de un tamaño pasado por teclado, el array contendrá números aleatorios entre 1 y 300 y mostrar aquellos números que acaben en un dígito que nosotros le indiquemos por teclado (debes controlar que se introduce un numero correcto), estos deben guardarse en un nuevo array.
Por ejemplo, en un array de 10 posiciones e indicamos mostrar los números acabados en 5, podría salir 155, 25, etc.
12. Crea un programa que pregunte al usuario si está lloviendo, en caso de responder “sí” pregunta si está haciendo mucho viento y si responde “sí” nuevamente muestra un mensaje indicando que hace mucho viento para salir con una sombrilla. En caso contrario, anima al usuario a que lleve una sombrilla. Para el caso de responder “no” en la primer pregunta, entonces solo desea un bonito día.
Considera que las respuestas pueden pasarse a minúsculas para evitar posibles errores y que los resultados se almacenen en un array.
13.  Crear un programa para el que el usuario indique un número entre 1 y 7. Como respuesta se le brindará un mensaje según el número leido:
1 - “Hoy aprenderemos sobre prorgamación”
2 - “¿Qué tal tomar un curso de marketing digital?
3 - “Hoy es un gran día para comenzar a aprender de diseño”
4 - ¿Y si aprendemos algo de negocios online?
5 - “Veamos un par de clases sobre producción audiovisual”
6 - “Tal vez sea bueno desarrollar una habilidad blanda”
7 - “Yo decido distraerme programando”
En caso indicar un número distinto, pedir al usuario que ingrese uno en el rango válido.
14. Leer un numero entre 1000 a 7000 y almacenar los digitos en un array e imprimirlos en pantalla de la siguiente manera:
```sh
Ingresa el número: 5501
Salida:
[3] = 5
[2] = 5
[1] = 0
[0] = 1
```
15. Crear un programa que permita convertit un número a números romanos.
16. Queremos guardar los nombres y la edades de los alumnos de un curso. Realiza un programa que introduzca el nombre y la edad de cada alumno. El proceso de lectura de datos terminará cuando se introduzca como nombre un asterisco (*) Al finalizar se mostrará los siguientes datos:
Todos lo alumnos mayores de edad.
Los alumnos mayores (los que tienen más edad)
17. Diseñar el algoritmo correspondiente a un programa, que:
Crea una tabla bidimensional de longitud nxn y nombre ‘diagonal’.
Carga la tabla de forma que los componentes pertenecientes a la diagonal de la matriz tomen el valor 1 y el resto el valor 0.
Muestra el contenido de la tabla en pantalla.
18. Empleando matrices desarrollar un programa que permita calcular la matriz transpuesta.
