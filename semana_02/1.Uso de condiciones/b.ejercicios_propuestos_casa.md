# Ejercicios propuestos - Casa

1. Crea una clase con un método main que pida una entrada de teclado y usando un bucle while, el método length de la clase String y el método substring de la clase String, muestre cada una de las letras que  componen la entrada. Por ejemplo si se introduce “ave” debe mostrar:

```sh
Letra 1: a
Letra 2: v
Letra 3: e
```

2. Construir un programa que visualice por pantalla todos los caracteres correspondientes a letras minúsculas.

3. Construir un programa que calcule el factorial de un valor numérico introducido como parámetro o argumento en la línea de comandos.

4. Construir un programa que calcule y visualice por pantalla el factorial de todos los valores numéricos enteros entre 1 y 10.

5. Construir un programa que visualice por pantalla los parámetros o argumentos de la linea de ejecución en orden inverso. Nota: debe emplearse un bucle while

6. Desarrollar un programa que permita ingresar una serie de numeros que se almacenen en un matriz y permita ordenar empleando el método de insersión.

7. Escribir un programa que imprima los primeros n términos de la sucesión de Ulam, donde n es un número que el usuario ingresa por teclado.

8. Escribir un programa que determine si un número es un número de Harshad o no utilizando un bucle while.

9. Escribir un programa que calcule la suma de los primeros n términos de una serie geométrica, donde n y los términos de la serie son ingresados por el usuario.

10. Escribe un programa que permita una serie de valores desde un archivo ".txt" y seguidamente lo almacene en un arreglo y permita calcular el promedio, moda y desviacion estandar, el algortimo debe ser eficiente, para esto optimice las iteresaciones.

```sh
Cree el archivo datos.txt con los siguientes datos

2 7 28 20 11 18 11 70 46 26 26 2 10 64 38 0 48 54 62 33 45 63 36 20 33 12 30 23 62 62 15 54 23 6 56 64 29 54 27 55 63 67 23 42 4 46 51 64 52 29 17 23 66 63 11 54 33 7 65 5 12 20 26 55 51 24 0 52
```

11. Una persona adquirió un producto para pagar en 20 meses. El primer mes pagó S/10,el segundo S/20, el tercero S/40 y así sucesivamente.
Realizar un programa para determinar cuánto debe pagar mensualmente y el total de lo que pagará después de los 20 meses.
Este programa puede servir para calcular otras compras y con diferentes plazos.

12. Crea un programa que muestre en pantalla los N primeros números primos. Se pide por teclado la cantidad de números primos que queremos mostrar.

13. Crear un cronometro que permita pintar en teclado la cuenta regresiva de los segundos ingresados empleando un bucle for.

14. Escribir un programa que pida al usuario un número entero positivo y muestre por pantalla todos los números impares desde 1 hasta ese número separados por puntos y comas.

15. Un pretamista solicita un prestamos de 5000 solaes a una tasa de interes mensual de 1.6% y con tiempo de pago de 18 meses, realizar un programa que permita visualiar los saldos en cada mes, hasta que se cancele la deuda.

16. Escribir un programa que pida al usuario un número entero y muestre por pantalla un triángulo rectángulo como el de más abajo.

```sh
1
3 1
5 3 1
7 5 3 1
9 7 5 3 1
```

17. Desarrollar un programa que permita ingresar un vector y permita calcular el máximo y mínimo valor.

18. Escribe un programa que pida el limite inferior y superior de un intervalo.  Si el límite inferior es mayor que el superior lo tiene que volver a pedir.  A continuación se van introduciendo números hasta que introduzcamos el 0.
Cuando termine el programa dará las siguientes informaciones:
– La suma de los números que están dentro del intervalo (intervalo abierto).
– Cuantos números están fuera del intervalo.
– Informa si hemos introducido algún número igual a los límites del intervalo.

19. Escribir un programa que imprima los primeros "N" números primos, donde n es un número que el usuario ingresa por teclado.

20. Escribir un programa que calcule la suma de los primeros "N" números perfectos, donde n es un número que el usuario ingresa por teclado.
