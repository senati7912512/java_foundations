# Ejercicios propuestos - Laboratorio

1. Escribir un programa que imprima la secuencia de Fibonacci hasta un número específico, utilizando un bucle while.

2. Desarrollar un programa que sume los "N" números impares.
   
3. Desarrollar un programa que permita convertir un numero en base decimal a otro sistema de numeración(binario, ternario, cuaternario, ..., octario, nonario)

4. Escribir un programa que calcule la suma de los primeros n números perfectos, donde "N" es un número que el usuario ingresa por teclado.

5. Mostrar los números primos desde 2 hasta "N"

6. Desarollar un programa que imprima la siguiente salida:

```sh
*
**
***
****
*****
******
*******
********
```

7. Desarrollar un programa que sume los "N" primeros pares empleando blucle do-while.

8. Escribir un programa que calcule el Máximo Común Divisor (MCD) de "N" números utilizando el ciclo while.

9. Escribir un programa que determine si un número es perfecto o no utilizando un bucle do-while.

10. Crea una aplicación que permita adivinar un número. La aplicación genera un número aleatorio del 1 al 100. A continuación va pidiendo números y va respondiendo si el número a adivinar es mayor o menor que el introducido, a demás de los intentos que te quedan (tienes 10 intentos para acertarlo).
El programa termina cuando se acierta el número (además te dice en cuantos intentos lo has acertado), si se llega al limite de intentos te muestra el número que había generado.

11. Desarrollar un programa que imprima la siguiente salida:
    
```sh
            *
           ***
          *****
         *******
          *****
           ***
            * 
```

12.  Escribir un programa que permita calcular el Mínimo Común Multiplo de "N" numeros enpleando el bucle for.

13.  Desarrollar un programa que lea por teclado "N" productos, los cuales deben ser almacenados en un arrar y permita vizualizarlos.

14.  Vacular el factorial de un numero empleando el bucle while.

15.  Escribir un programa que imprima los números de la secuencia de Lucas hasta un número específico, utilizando un bucle while.
16.  Escribir un programa que imprima los "N" numeros y la salida sea de la siguirente manera

```sh
1 2 3 4 5 ... N
```

17. Escriba un programa que dibuje un cuadrado donde los lados se pintarán de la siguiente forma:

```sh
Por ejemplo si el lado es 7, se debe dibujar de la siguiente forma:

* * * * * * *
* * * * * * * 
* * * * * * *
* * * * * * *
* * * * * * *
* * * * * * *
* * * * * * *
```

18. Escribir un programa que imprima los números del 1 al 100, pero para los múltiplos de 3 imprima "Fizz" en su lugar, y para los múltiplos de 5 imprima "Buzz". Para los números que son múltiplos tanto de 3 como de 5, imprima "FizzBuzz".
