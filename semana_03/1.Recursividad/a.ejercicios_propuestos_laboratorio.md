# Ejercicios propuestos - Laboratorio

> Empleando recursividad, lectura de datos por teclado e implementando métodos de lectura, cálculo en una clase y heredando la clase main desarrollar los siguientes ejercicios:

1. Calcular el n-ésimo término de la serie de Fibonacci.
2. Calcular el máximo común divisor de N números.
3. Calcular el mínimo común múltiplo de N números.
4. Calcular el número de permutaciones de N elementos de una lista.
5. Crear una clase "Fractal" que tenga como atributos el tipo de fractal y los parámetros necesarios para su construcción, y como métodos la construcción del fractal utilizando recursividad.
6. Crear un programa que calcule el valor, máximo, mínimo, el promedio, la moda y la desviación estandar de 1000 datos que se generen aleatoriamente con un rango de 0 a 300 elementos, los datos deben ser generados en un archivo datos_generados.txt,seguidamente se debe generar y los resultados para ser guardados en un archivo resultados.txt
7. Desarrollar un algoritmo recursivo para realizar un ordenamiento de los datos generados empleando "QuickSort".
8. Crear una clase Persona y una clase heredada Trabajador que permita almacemar en un arreglo los campos de "Nombre completo" y "Grupo de personas" desde el archivo "clientes.xls", seguidamente realizar el ordenamiento de los registros y guardar los resultados en "datos_procesados.txt"
