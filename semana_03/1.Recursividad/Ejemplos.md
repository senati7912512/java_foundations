# Ejemplo de recursividad

```java
import java.util.Scanner;
import java.math.BigInteger;

public class fibonacci_mejorado {
    public static void main(String[] args) {
        Scanner leer = new Scanner(System.in);

        System.out.print("Indique el limite de la funcion de fibonacci: ");
        int n = leer.nextInt();
        for(int i = 0 ; i <= n ; i++){
            System.out.println(">> Item: " + i + " -- " + fibonacci(i));
        }
    }

    public static BigInteger fibonacci(int n) {
        BigInteger[] posicion = new BigInteger[3];
        posicion[0] = BigInteger.ONE;
        posicion[1] = BigInteger.ZERO;
        return fibonacci(n, posicion);
    }

    private static BigInteger fibonacci(int n, BigInteger[] posicion) {
        if (n <= 0) {
            return BigInteger.ZERO;
        }
        if (n <= 2) {
            return BigInteger.ONE;
        } else {
            posicion[2] = posicion[1].add(posicion[0]);
            posicion[1] = posicion[0];
            posicion[0] = posicion[2];
            fibonacci(n - 1, posicion);
            return posicion[0].add(posicion[1]);
        }
    }
}

```

# Ejemplo de lectura y escritura de archivos en java

```java
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class LeerEscribirArchivo {

    public static void main(String[] args) {
        
        // Abre el archivo para leer
        File archivoLectura = new File("archivo.txt");
        Scanner lector;
        try {
            lector = new Scanner(archivoLectura);
        } catch (FileNotFoundException e) {
            System.out.println("Archivo no encontrado.");
            return;
        }
        
        // Lee el contenido del archivo
        while (lector.hasNextLine()) {
            String linea = lector.nextLine();
            System.out.println(linea);
        }
        lector.close();
        
        // Abre el archivo para escribir
        File archivoEscritura = new File("archivo.txt");
        PrintWriter escritor;
        try {
            escritor = new PrintWriter(archivoEscritura);
        } catch (FileNotFoundException e) {
            System.out.println("Archivo no encontrado.");
            return;
        }
        
        // Escribe en el archivo
        escritor.println("¡Los de SENATI son unos tigres en programación!");
        escritor.println("Este es un ejemplo de escritura en un archivo de texto.");
        escritor.close();
        
        System.out.println("Archivo actualizado.");
    }

}
```

```java
import java.util.Scanner;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class GeneradorAleatorioRecursivo {
    private static final int LIMITE_SUPERIOR = 300; //Límite superior del rango de los aleatorios
    private static final String NOMBRE_ARCHIVO = "aleatorios.txt"; //Nombre del archivo de texto

    public static void main(String[] args) {
        Scanner leer = new Scanner(System.in);

        System.out.print("Indique, ¿cuántos números se generarán?: ");
        int n = leer.nextInt();

        int[] aleatorios = generarAleatorios(n, LIMITE_SUPERIOR);
        guardarEnArchivo(aleatorios, NOMBRE_ARCHIVO);
        leer.close();
    }
    
    //Método para generar n números aleatorios recursivamente
    public static int[] generarAleatorios(int n, int maximo) {
        int[] aleatorios = new int[n];
        generarAleatoriosRecursivo(n, maximo, aleatorios, 0);
        return aleatorios;
    }
    
    private static void generarAleatoriosRecursivo(int n, int maximo, int[] aleatorios, int indice) {
        if (n == 0) {
            return;
        }
        Random random = new Random();
        int aleatorio = random.nextInt(maximo);
        aleatorios[indice] = aleatorio;
        generarAleatoriosRecursivo(n - 1, maximo, aleatorios, indice + 1);
    }
    
    //Método para guardar un arreglo de enteros en un archivo de texto
    public static void guardarEnArchivo(int[] numeros, String nombreArchivo) {
        try {
            FileWriter escritor = new FileWriter(nombreArchivo);
            for (int numero : numeros) {
                escritor.write(numero + " ");
            }
            escritor.close();
            System.out.println("Los números se han guardado en el archivo " + nombreArchivo + ".");
        } catch (IOException e) {
            System.out.println("Error al guardar en archivo.");
            e.printStackTrace();
        }
    }

}
```

```java
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class LecturaRecursiva {

    public static void leerArchivo(String archivo) {
        try (BufferedReader br = new BufferedReader(new FileReader(archivo))) {
            String linea;
            while ((linea = br.readLine()) != null) {
                System.out.println(linea);
            }
        } catch (IOException e) {
            System.out.println("Error al leer el archivo " + archivo);
        }
    }

    public static void main(String[] args) {
        String archivo = "aleatorios.txt";
        leerArchivo(archivo);
    }
}
```
