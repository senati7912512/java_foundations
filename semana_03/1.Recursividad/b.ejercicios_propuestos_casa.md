# Ejercicios propuestos - Casa

## Empleado recursividad resolver los siguientes ejercicios

1. Calcular la potencia de un número entero positivo.
2. Calcular el máximo valor en un array de enteros.
3. Calcular el enésimo término de la secuencia de Ackermann.
4. Imprimir los números del 1 al n en orden inverso.
5. Crear un programa que dibuje el siguiente gráfico.

```sh

       *
      **
     ***
    ****
   *****
  ******
 *******   
```
