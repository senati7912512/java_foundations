# Ejercicios propuestos - Laboratorio

1. Realizar un programa que calcule el area y perímetro de una elipse.
2. Determinar si un número entero es primo.
3. Calcular el valor de la hipotenusa de un triángulo rectángulo a partir de sus dos catetos.
4. Calcular el área de un triángulo a partir de sus tres lados.
5. Calcular el perímetro de un polígono regular a partir de su número de lados y su longitud de lado.
6. Calcular la raíz cuadrada de un número entero.
7. Calcular el valor absoluto de un número entero.
8. Calcular el área de un trapecio a partir de sus cuatro lados.
9. Realizar un algoritmo que permita verificar si los lados ingresados forman y triangulo rectángulo, si cumple con esta condicion permita imprimir sus razones trigonométricas.
10. Realizar un programa que permita representar un polinomio de grado n donde(n <= 7) y los coeficientes y el grado deben ser ingresador por teclado y la parte literar sea expresado como "X".
11. La compañía Intcomex Perú S.A.C. paga a su personal de ventas un salario de S/2500.00 mensuales más una comisión de S/50.00 por cada equipo exclusivo de la compañía, más el 7% del valor de la venta. Cada mes el contador de la empresa ingresa el nombre del vendedor, el número de productos vendidos y el valor total facturado. Calcule y escriba el salario total de un vendedor
12. En un hospital existen 3 áreas: Oncología, Pediatría y Traumatología. El presupuesto anual del hospital se reparte de la siguiente manera:
Área Presupuesto:

```sh
| Área            | Presupuesto |
| ---             | ---         |
| Oncología       | 55%         |
| Pedriatría      | 20%         |
| Traumatología   | 25%         |
```

13.  Escriba un programa que realice la conversión de grados Celsius (°C) a grados Fahrenheit (°F). La ecuación de conversión es: 
    $F = (9*C/5) + 32$
14.  Determine el área de la superficie lateral y el volumen de un cilindro conocido su radio y altura.

```sh
Superficie = 2 * pi * r * h
Volumen = pi * r ^ 2 * h
```

15.  Desarrolla un programa que ayude a una cajera a identificar el número de billetes y monedas que se necesitan de cada una de las siguientes denominaciones 200, 100, 50, 20, 10, 5, 2 y 1 para una cantidad dada. Ejemplo si la cantidad es 1390, se necesitan 6 billetes de 200, 1 billete de 100, 1 billete de 50 y 2 billetes de 20.
16.  . Ingresar 2 números y luego un carácter indicando la operación a realizar (+,-,*,/,^) y reportar el resultado de la operación.
17.  En una playa de estacionamiento cobran S/. 2.5 por hora o fracción. Diseñe un algoritmo que determine cuanto debe
pagar un cliente por el estacionamiento de su vehículo, conociendo el tiempo de estacionamiento en horas y minutos.
18.  Una tienda ofrece un descuento del 15% sobre el total de la compra y un cliente desea saber cuánto deberá pagar finalmente por su compra.
