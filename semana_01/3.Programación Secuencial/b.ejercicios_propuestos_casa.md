# Ejercicios propuestos - Casa

1. Programa Java que lea dos números enteros por teclado y los muestre por pantalla.
2. Programa Java que lea un nombre y muestre por pantalla: “Buenos dias nombre_introducido”.
3. Programa Java que lee un número entero por teclado y obtiene y muestra por pantalla el doble y el triple de ese número.
4. Programa que lea una cantidad de grados centígrados y la pase a grados Fahrenheit. La fórmula correspondiente es: F = 32 + ( 9 * C / 5)
5. Programa que lee por teclado el valor del radio de una circunferencia y calcula y muestra por pantalla la longitud y el área de la circunferencia. Longitud de la circunferencia = 2*PI*Radio, Area de la circunferencia = PI*Radio^2  
6. Programa que pase una velocidad en Km/h a m/s. La velocidad se lee por teclado.
7. Programa lea la longitud de los catetos de un triángulo rectángulo y calcule la longitud de la hipotenusa según el teorema de Pitágoras.
8. Programa que calcula el volumen de una esfera.
9. Programa que calcula el área de un triángulo a partir de la longitud de sus lados.
10. Programa que lee un número de 3 cifras y muestra sus cifras por separado.
11. Programa que lea un número entero N de 5 cifras y muestre sus cifras desde el principio como en el ejemplo.
12. Programa que lea un número entero N de 5 cifras y muestre sus cifras desde el final igual que en el ejemplo.
13. Programa que calcula el número de la suerte de una persona a partir de su fecha de nacimiento.
14. Programa para calcular el precio final de venta de un producto.
15. Programa quite a una variable N sus m últimas cifras.
16. Programa para pasar de grados centígrados a grados Kelvin y grados Reamur.
17. Pasar de grados centígrados a grados kelvin.El proceso de leer grados centígrados se debe repetir mientras que se responda ‘S’ a la pregunta: Repetir proceso? (S/N)
18. Mostrar la tabla de multiplicar de un número.
19. Leer números y contar cuántos acaban en 2.
20. Comprobar si dos números son amigos.
