# Ejercicios propuestos - Casa

Implementar los siguientes programas que lean las listas indicadas y realizen lo solicitado:

1. Números enteros y determine si hay algún par de números cuya suma sea igual a un número determinado.
2. Palabras y determine cuántas de ellas tienen más de dos vocales consecutivas.
3. Números enteros y determine cuántos de ellos son números perfectos y cuántos son números de Fibonacci.
4. Números enteros y determine cuántos de ellos son divisibles por todos los números de 1 a 10.
5. Palabras y determine cuántas de ellas contienen todas las vocales.
6. Números enteros y determine si hay algún número que es divisible por todos los números de 1 a 20.
7. Palabras y determine cuántas de ellas tienen la misma letra repetida tres veces consecutivas.
8. Números enteros y determine si hay algún número que sea divisible por la suma de los dígitos de todos los números de 1 a 100.
9. Palabras y determine cuántas de ellas tienen una letra repetida un número impar de veces.
10. Números enteros y determine cuántos de ellos tienen la propiedad de ser iguales a la suma de los dígitos factoriales de su descomposición en dígitos.
11. Palabras y determine cuántas de ellas tienen la misma letra en las posiciones 1 y 4.
12. Números enteros y determine cuántos de ellos son números de Smith.
13. Palabras y determine cuántas de ellas tienen una letra que aparece más de dos veces pero menos de cinco veces.
14. Números enteros y determine cuántos de ellos son números de Kaprekar.
15. Palabras y determine cuántas de ellas tienen la misma letra al principio y al final.
16. Números enteros y determine cuántos de ellos son números de Armstrong.
17. Palabras y determine cuántas de ellas tienen una letra que aparece exactamente dos veces en cualquier posición.
18. Números enteros y determine cuántos de ellos son números de Harshad.
19. Palabras y determine cuántas de ellas tienen todas las letras diferentes.
20. Números enteros y determine cuántos de ellos son números de narcisismo.
