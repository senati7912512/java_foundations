# Ejercicios propuestos - Laboratorio

1. Implementar un programa que lea un número entero y determine si es par o impar.
2. Implementar un programa que lea un número entero y determine si es positivo, negativo o cero.
3. Implementar un programa que lea tres números enteros y determine cuál es el mayor de ellos.
4. Implementar un programa que lea tres números enteros y determine si pueden formar un triángulo válido.
5. Implementar un programa que lea la edad de una persona y determine si es mayor de edad o no.
6. Implementar un programa que lea el peso y la estatura de una persona y calcule su índice de masa corporal (IMC).
7. Implementar un programa que lea una cadena de caracteres y determine si es un palíndromo o no.
8. Implementar un programa que lea una fecha en formato dd/mm/yyyy y determine si es válida o no.
9. Implementar un programa que lea un número entero y determine si es un número primo o no.
10. Implementar un programa que lea una lista de números enteros y determine cuántos de ellos son pares.
11. Impl8ementar un programa que lea una lista de números enteros y determine cuántos de ellos son múltiplos de 3.
12. Implementar un programa que lea una lista de números enteros y determine cuántos de ellos son números perfectos.
13. Implementar un programa que lea una lista de números enteros y determine cuántos de ellos son números de Fibonacci.
14. Implementar un programa que lea una lista de palabras y determine cuántas de ellas son palíndromos.
15. Implementar un programa que lea una lista de palabras y determine cuántas de ellas comienzan con una letra determinada.
16. Implementar un programa que lea una lista de palabras y determine cuántas de ellas contienen una letra determinada.
17. Implementar un programa que lea una lista de palabras y determine cuántas de ellas tienen una longitud mayor que un número determinado.
18. Implementar un programa que lea una lista de palabras y determine cuántas de ellas tienen una longitud menor que un número determinado.
19. Implementar un programa que lea un número entero y determine si es un número armónico o no.
20. Implementar un programa que lea una lista de números enteros y determine cuántos de ellos tienen una cantidad de divisores mayor que un número determinado.
