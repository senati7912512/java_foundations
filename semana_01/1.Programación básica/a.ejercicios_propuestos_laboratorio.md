# Ejercicios propuestos - Laboratorio

1. Crea un programa que imprima "Hola, mundo!" en la consola.
2. Crea un programa que permita al usuario ingresar su nombre y lo salude en la consola.
3. Crea un programa que permita al usuario ingresar dos números enteros y los sume.
4. Crea un programa que permita al usuario ingresar dos números enteros y los multiplique.
5. Crea un programa que permita al usuario ingresar dos números enteros y calcule su diferencia.
6. Crea un programa que permita al usuario ingresar dos números enteros y calcule su cociente y su resto.
7. Crea un programa que permita al usuario ingresar la base y la altura de un triángulo y calcule su área.
8. Crea un programa que permita al usuario ingresar la longitud de los lados de un triángulo y determine si es equilátero, isósceles o escaleno.
9. Crea un programa que permita al usuario ingresar el radio de un círculo y calcule su área y su circunferencia.
10. Crea un programa que permita al usuario ingresar un número entero y determine si es par o impar.
11. Crea un programa que permita al usuario ingresar dos números enteros y determine cuál de los dos es el mayor.
12. Crea un programa que permita al usuario ingresar dos números enteros y determine si son iguales o diferentes.
13. Crea un programa que permita al usuario ingresar una cadena de texto y la imprima en la consola.
14. Crea un programa que permita al usuario ingresar su edad y determine si es mayor de edad o no.
15. Crea un programa que permita al usuario ingresar un número entero y determine si es primo o no.
16. Crea un programa que permita al usuario ingresar su peso y su altura y calcule su índice de masa corporal (IMC).
17. Crea un programa que permita al usuario ingresar un número entero y determine si es positivo, negativo o cero.
18. Crea un programa que permita al usuario ingresar un número entero y calcule su factorial.
19. Crea un programa que permita al usuario ingresar un número entero y calcule la suma de todos los números enteros desde 1 hasta ese número.
20. Crea un programa que permita al usuario ingresar dos números enteros y calcule su Máximo Común Divisor (MCD) y su Mínimo Común Múltiplo (MCM).