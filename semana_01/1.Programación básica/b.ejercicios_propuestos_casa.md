# Ejercicios propuestos - Casa

1. Crea una variable de tipo "ArrayList" que contenga una lista de nombres de personas.
2. Crea una variable de tipo "HashMap" que contenga una lista de nombres de personas y su edad.
3. Crea una variable de tipo "Date" que contenga la fecha y hora actuales.
4. Crea una variable de tipo "Calendar" que contenga la fecha actual.
5. Crea una variable de tipo "File" que contenga la ruta de un archivo.
6. Crea una variable de tipo "Scanner" que permita leer la entrada del usuario por consola.
7. Crea una variable de tipo "BufferedReader" que permita leer un archivo de texto.
8. Crea una variable de tipo "Random" que permita generar números aleatorios.
9. Crea una variable de tipo "Math" que permita realizar operaciones matemáticas.
10. Crea una variable de tipo "Pattern" que permita realizar operaciones con expresiones regulares.
11. Crea una variable de tipo "Matcher" que permita buscar y encontrar patrones en una cadena.
12. Crea una variable de tipo "DateFormat" que permita dar formato a fechas y horas.
13. Crea una variable de tipo "SimpleDateFormat" que permita dar formato a fechas y horas personalizado.
14. Crea una variable de tipo "BigDecimal" que permita realizar operaciones matemáticas con números de alta precisión.
15. Crea una variable de tipo "BigInteger" que permita realizar operaciones matemáticas con números enteros de alta precisión.
16. Crea una variable de tipo "TimeZone" que permita definir una zona horaria.
17. Crea una variable de tipo "Locale" que permita definir una configuración regional.
18. Crea una variable de tipo "InputStream" que permita leer datos de entrada.
19. Crea una variable de tipo "OutputStream" que permita escribir datos de salida.
20. Crea una variable de tipo "Socket" que permita establecer una conexión de red.