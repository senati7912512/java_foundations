# Ejercicios propuestos - Laboratorio

1. Empleando los siguientes codigos desarrollar un login en consola que permita ingresa un usuario y una contraseña, la condicion es si ingresa tres intentos, el inicio de sesion pueda estar inactivo por quince segundos.

## Primer código

```java
import java.io.Console;

class Password {

    public void OcultarPassword() {
        Console console = System.console();
        if (console == null) {
            System.out.println("No se pudo obtener la instancia de la consola");
            System.exit(0);
        }

        char[] passwordArray = console.readPassword("Contraseña: ");
        for (int i = 0; i < passwordArray.length; i++) {
            System.out.print("*");
        }
        System.out.println();
        console.printf("Contraseña ingresada: \n", new String(passwordArray));

    }

    public static void main(String[] args) {
        new Password().OcultarPassword();
    }
}
```

## Segundo código

```java
import java.util.Scanner;

public class JavaConsoleLogin {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Usuario: ");
        String username = input.nextLine();
        System.out.print("Contraseña: ");
        String password = input.nextLine();
        System.out.println("Usuario: " + username);
        System.out.println("Contraseña: " + password);
        if (username.equals(username)) {
            if (password.equals("admin123")) {
                System.out.println("Inicia sesión correctamente [:)] ...");
            } else {
                System.out.println("Contraseña incorrecta [:(]...");
            }
        }
        input.close();
    }
}
```

## Se requiere un programa que modele una cuenta bancaria que posee los siguientes atributos:

* Nombres del titular.
* Apellidos del titular.
* Número de la cuenta bancaria.
* Tipo de cuenta: puede ser una cuenta de ahorros o una cuenta corriente.
* Saldo de la cuenta.

Se debe definir un constructor que inicialice los atributos de la clase.
Cuando se crea una cuenta bancaria, su saldo inicial tiene un valor de cero.

En una determinada cuenta bancaria se puede:

* Imprimir por pantalla los valores de los atributos de una cuenta bancaria.
* Consultar el saldo de una cuenta bancaria.
* Consignar un determinado valor en la cuenta bancaria, actualizando el saldo correspondiente.
* Retirar un determinado valor de la cuenta bancaria, actualizando el saldo correspondiente. Es necesario tener en cuenta que no se puede realizar el retiro si el valor solicitado supera el saldo actual de la cuenta.

## Clase: CuentaBancaria

```java
/**
 * Esta clase define objetos que representan una cuenta bancaria que
 * tienen un nombre y apellidos del titular, un número de cuenta, un
 * tipo de cuenta (ahorros o corriente) y un saldo.
 * 
 * @version 1.2/2023
 */

public class CuentaBancaria {
    // Atributo que define los nombres del titular de la cuenta bancaria
    String nombresTitular;

    // Atributo que define los apellidos del titular de la cuenta bancaria
    String apellidosTitular;

    // Atributo que define el número de la cuenta bancaria
    int numeroCuenta;

    // Tipo de cuenta como un valor enumerado
    enum tipo {
        AHORROS, CORRIENTE
    }

    // Atributo que define el tipo de cuenta bancaria
    tipo tipoCuenta;

    /*
     * Atributo que define el saldo de la cuenta bancaria con valor inicial
     * cero
     */
    static float saldo = 0;

    /**
     * Constructor de la clase CuentaBancaria
     * 
     * @param nombresTitular   Parámetro que define los nombres del
     *                         titular de una cuenta bancaria
     * @param apellidosTitular Parámetro que define los apellidos del
     *                         titular de una cuenta bancaria
     * @param numeroCuenta     Parámetro que define el número de una
     *                         cuenta bancaria
     * @param tipoCuenta       Parámetro que define el tipo de una cuenta
     *                         bancaria (puede ser ahorros o corriente)
     */
    CuentaBancaria(String nombresTitular, String apellidosTitular, int numeroCuenta, tipo tipoCuenta) {
        /*
         * Tener en cuenta que no se pasa como parámetro el saldo ya
         * que inicialmente es cero.
         */
        this.nombresTitular = nombresTitular;
        this.apellidosTitular = apellidosTitular;
        this.numeroCuenta = numeroCuenta;
        this.tipoCuenta = tipoCuenta;
    }

    /**
     * Método que imprime en pantalla los datos de una cuenta bancaria
     */
    void imprimir() {
        System.out.println("Nombres del titular = " + nombresTitular);
        System.out.println("Apellidos del titular = " + apellidosTitular);
        System.out.println("Número de cuenta = " + numeroCuenta);
        System.out.println("Tipo de cuenta = " + tipoCuenta);
        System.out.println("Saldo = " + saldo);
    }

    /**
     * Método que imprime en pantalla el saldo actual de una cuenta
     * bancaria
     */
    void consultarSaldo() {
        System.out.println("El saldo actual es = " + saldo);
    }

    /**
     * Método que actualiza y devuelve el saldo de una cuenta bancaria a
     * partir de un valor a consignar
     * 
     * @param valor Parámetro que define el valor a consignar en la
     *              cuenta bancaria. El valor debe ser mayor que cero
     * @return Valor booleano que indica si el valor a consignar es válido
     *         o no
     */
    boolean consignar(int valor) {
        // El valor a consignar debe ser mayor que cero
        if (valor > 0) {
            saldo = saldo + valor; /* Se actualiza el saldo de la cuenta con el valor consignado */
            System.out.println("Se ha consignado $" + valor + " en la cuenta. El nuevo saldo es $" + saldo);
            return true;
        } else {
            System.out.println("El valor a consignar debe ser mayor que cero.");
            return false;
        }
    }

    /**
     * Método que actualiza y devuelve el saldo de una cuenta bancaria a
     * partir de un valor a retirar
     * 
     * @param valor Parámetro que define el valor a retirar en la cuenta
     *              bancaria. El valor debe ser mayor que cero y el saldo de la
     *              cuenta
     *              debe quedar con un valor positivo o igual a cero
     * @return Valor booleano que indica si el valor a retirar es válido o no
     */
    boolean retirar(int valor) {
        /* El valor debe ser mayor que cero y no debe superar el saldo actual */
        if ((valor > 0) && (valor <= saldo)) {
            saldo = saldo - valor; /* Se actualiza el saldo de la cuenta con el valor retirado */
            System.out.println("Se ha retirado $" + valor + " en la cuenta. El nuevo saldo es $" + saldo);
            return true;
        } else {
            System.out.println("El valor a retirar debe ser menor que el saldo actual.");
            return false;
        }
    }

    /**
     * Método main que crea una cuenta bancaria sobre las cuales se
     * realizan las operaciones de consignar y retirar
     */
    public static void main(String args[]) {
        CuentaBancaria cuenta = new CuentaBancaria("Pedro", "Pérez", 123456789, tipo.AHORROS);
        cuenta.imprimir();
        cuenta.consignar(200000);
        cuenta.consignar(300000);
        cuenta.retirar(400000);
    }
}
```


