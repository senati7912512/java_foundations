# Ejercicios propuestos - Casa

## Se requiere un programa que modele el concepto de un automóvil. Un automóvil tiene los siguientes atributos:

* Marca: el nombre del fabricante.
* Modelo: año de fabricación.
* Motor: volumen en litros del cilindraje del motor de un automóvil.
* Tipo de combustible: valor enumerado con los posibles valores de gasolina, bioetanol, diésel, biodiésel, gas natural.
* Tipo de automóvil: valor enumerado con los posibles valores de carro de ciudad, subcompacto, compacto, familiar, ejecutivo, SUV.
* Número de puertas: cantidad de puertas.
* Cantidad de asientos: número de asientos disponibles que tiene el vehículo.
* Velocidad máxima: velocidad máxima sostenida por el vehículo en km/h.
* Color: valor enumerado con los posibles valores de blanco, negro, rojo, naranja, amarillo, verde, azul, violeta.
* Velocidad actual: velocidad del vehículo en un momento dado.
La clase debe incluir los siguientes métodos:
* Un constructor para la clase Automóvil donde se le pasen como parámetros los valores de sus atributos.
* Métodos get y set para la clase Automóvil.
* Métodos para acelerar una cierta velocidad, desacelerar y frenar (colocar la velocidad actual en cero). Es importante tener en cuenta que no se debe acelerar más allá de la velocidad máxima permitida para el automóvil. De igual manera, tampoco es posible  desacelerar a una velocidad negativa. Si se cumplen estos casos, se debe mostrar por pantalla los mensajes correspondientes.
* Un método para calcular el tiempo estimado de llegada, utilizando como parámetro la distancia a recorrer en kilómetros. El tiempo estimado se calcula como el cociente entre la distancia a recorrer y la velocidad actual.
* Un método para mostrar los valores de los atributos de un Automóvil en pantalla.
* Un método main donde se deben crear un automóvil, colocar su velocidad actual en 100 km/h, aumentar su velocidad en 20 km/h, luego decrementar su velocidad en 50 km/h, y después frenar. Con cada cambio de velocidad, se debe mostrar en pantalla la velocidad actual

```java
/**

* Esta clase define objetos de tipo Automóvil con una marca, modelo,
* motor, tipo de combustible, tipo de automóvil, número de puertas,
* cantidad de asientos, velocidad máxima, color y velocidad actual.
* @version 1.2/2023
*/
package ejercicios;

public class Automovil {
    // Atributo que define la marca de un automóvil
    String marca;

    // Atributo que define el modelo de un automóvil
    int modelo;

    // Atributo que define el motor de un automóvil
    int motor;

    // Tipo de combustible como un valor enumerado
    enum tipoCom {GASOLINA, BIOETANOL, DIESEL, BIODISESEL, GAS_NATURAL}
    
    // Atributo que define el tipo de combustible
    tipoCom tipoCombustible;
    
    // Tipo de automóvil como un valor enumerado
    enum tipoA {CIUDAD, SUBCOMPACTO, COMPACTO, FAMILIAR, EJECUTIVO, SUV}
    
    // Atributo que define el tipo de automóvil
    tipoA tipoAutomovil;
    
    // Atributo que define el número de puertas de un automóvil
    int numeroPuertas;
    
    // Atributo que define la cantidad de asientos de un automóvil
    int cantidadAsientos;
    
    // Atributo que define la velocidad máxima de un automóvil
    int velocidadMaxima;
    
    // Color del automóvil como un valor enumerado
    enum tipoColor {BLANCO, NEGRO, ROJO, NARANJA, AMARILLO, VERDE, AZUL, VIOLETA}
    
    // Atributo que define el color de un automóvil
    tipoColor color;
    
    // Atributo que define la velocidad de un automóvil
    int velocidadActual = 0;

    /**
     * Constructor de la clase Automóvil
     * 
     * @param marca            Parámetro que define la marca de un automóvil
     * @param modelo           Parámetro que define el modelo (año de
     *                         fabricación) de un automóvil
     * @param motor            Parámetro que define el volumen del cilindraje del
     *                         motor (puede ser gasolina, bioetanol, diésel,
     *                         biodiesel o gas natural)
     * @param tipoAutomovil    Parámetro que define el tipo de automóvil
     *                         (puede ser Carro de ciudad, Subcompacto, Compacto,
     *                         Familiar,
     *                         Ejecutivo o SUV)
     * @param numeroPuertas    Parámetro que define el número de
     *                         puertas de un automóvil
     * @param cantidadAsientos Parámetro que define la cantidad de
     *                         asientos que tiene el automóvil
     * @param velocidadMaxima  Parámetro que define la velocidad
     *                         máxima permitida al automóvil
     * @param color            Parámetro que define el color del automóvil (puede
     *                         ser Blanco, Negro, Rojo, Naranja, Amarillo, Verde,
     *                         Azul o Violeta)
     */
    Automovil(String marca, int modelo, int motor, tipoCom tipoCombustible, tipoA tipoAutomovil, int numeroPuertas, int cantidadAsientos, int velocidadMaxima, tipoColor color) {
        this.marca = marca;
        this.modelo = modelo;
        this.motor = motor;
        this.tipoCombustible = tipoCombustible;
        this.tipoAutomovil = tipoAutomovil;
        this.numeroPuertas = numeroPuertas;
        this.cantidadAsientos = cantidadAsientos;
        this.velocidadMaxima = velocidadMaxima;
        this.color = color;
    }

    /**
     * Método que devuelve la marca de un automóvil
     * 
     * @return La marca de un automóvil
     */
    String getMarca() {
        return marca;
    }

    /**
    * Método que devuelve el modelo de un automóvil
    * @return El modelo de un automóvil
    */
    int getModelo() {
        return modelo;
    }

    /**
     * Método que devuelve el volumen en litros del cilindraje del motor
     * de un automóvil
     * 
     * @return El volumen en litros del cilindraje del motor de un
     *         automóvil
     */
    int getMotor() {
        return motor;
    }

    /**
     * Método que devuelve el tipo de combustible utilizado por el motor
     * de un automóvil
     * 
     * @return El tipo de combustible utilizado por el motor de un
     *         automóvil
     */
    tipoCom getTipoCombustible() {
        return tipoCombustible;
    }

    /**
     * Método que devuelve el tipo de automóvil
     * 
     * @return El tipo de automóvil
     */
    tipoA getTipoAutomovil() {
        return tipoAutomovil;
    }

    /**
     * Método que devuelve el número de puertas de un automóvil
     * 
     * @return El número de puertas que tiene un automóvil
     */
    int getNumeroPuertas() {
        return numeroPuertas;
    }

    /**
    * Método que devuelve la cantidad de asientos de un automóvil
    * @return La cantidad de asientos que tiene un automóvil
    */
    int getCantidadAsientos() {
        return cantidadAsientos;
    }
    
    /**
    * Método que devuelve la velocidad máxima de un automóvil
    * @return La velocidad máxima de un automóvil
    */
    int getVelocidadMaxima() {
        return velocidadMaxima;
    }
    
    /**
    * Método que devuelve el color de un automóvil
    * @return El color de un automóvil
    */
    tipoColor getColor() {
        return color;
    }
    
    /**
    * Método que devuelve la velocidad actual de un automóvil
    * @return La velocidad actual de un automóvil
    */
    int getVelocidadActual() {
        return velocidadActual;
    }
    
    /**
    * Método que establece la marca de un automóvil
    * @param marca Parámetro que define la marca de un automóvil 
    */
    void setMarca(String marca) {
        this.marca = marca;
    }
    
    /**
    * Método que establece el modelo de un automóvil
    * @param modelo Parámetro que define el modelo de un automóvil 
    */
    void setModelo(int modelo) {
        this.modelo = modelo;
    }
    
    /**
    * Método que establece el volumen en litros del motor de un automóvil
    * @param motor Parámetro que define el volumen en litros del 
    * motor de un automóvil 
    */
    void setMotor(int motor) {
        this.motor = motor;
    }

    /**
    * Método que establece el tipo de combustible de un automóvil
    * @param tipoCombustible Parámetro que define el tipo de 
    * combustible de un automóvil 
    */
    void setTipoCombustible(tipoCom tipoCombustible) {
        this.tipoCombustible = tipoCombustible;
    }
    
    /**
    * Método que establece el tipo de automóvil
    * @param tipoAutomovil Parámetro que define el tipo de automóvil 
    */
    void setTipoAutomovil(tipoA tipoAutomovil) {
        this.tipoAutomovil = tipoAutomovil;
    }
    
    /**
    * Método que establece el número de puertas de un automóvil
    * @param numeroPuertas Parámetro que define el número de 
    * puertas de un automóvil 
    */
    void setNumeroPuertas(int numeroPuertas) {
        this.numeroPuertas = numeroPuertas;
    }
    
    /**
    * Método que establece la cantidad de asientos de un automóvil
    * @param cantidadAsientos Parámetro que define la cantidad de 
    * asientos de un automóvil 
    */
    void setCantidadAsientos(int cantidadAsientos) {
        this.cantidadAsientos = cantidadAsientos;
    }

    /**
     * Método que establece la velocidad máxima de un automóvil
     * 
     * @param velocidadMaxima Parámetro que define la velocidad
     *                        máxima de un automóvil
     */
    void setVelocidadMaxima(int velocidadMaxima) {
        this.velocidadMaxima = velocidadMaxima;
    }

    /**
     * Método que establece el color de un automóvil
     * 
     * @param color Parámetro que define el color de un automóvil
     */
    void setColor(tipoColor color) {
        this.color = color;
    }

    /**
     * Método que establece la velocidad de un automóvil
     * 
     * @param velocidadActual Parámetro que define la velocidad actual
     *                        de un automóvil
     */
    void setVelocidadActual(int velocidadActual) {
        this.velocidadActual = velocidadActual;
    }

    /**
    * Método que incrementa la velocidad de un automóvil
    * @param incrementoVelocidad Parámetro que define la cantidad a 
    * incrementar en la velocidad actual de un automóvil 
    */
    void acelerar(int incrementoVelocidad) {
        if (velocidadActual + incrementoVelocidad < velocidadMaxima) {
            /* Si el incremento de velocidad no supera la velocidad 
            máxima */
            velocidadActual = velocidadActual + incrementoVelocidad;
        } else { /* De otra manera no se puede incrementar la velocidad y 
            se genera mensaje */
            System.out.println("No se puede incrementar a una velocidad superior a la máxima del automóvil.");
        }
    }
    
    /**
     * Método que decrementa la velocidad de un automóvil
     * 
     * @param marca Parámetro que define la cantidad a decrementar en
     *              la velocidad actual de un automovil
     */
    void desacelerar(int decrementoVelocidad) {
        /* La velocidad actual no se puede decrementar alcanzando un 
        valor negativo */
        if ((velocidadActual - decrementoVelocidad) > 0) {
            velocidadActual = velocidadActual - decrementoVelocidad;
        } else { /* De otra manera no se puede decrementar la velocidad y 
        se genera mensaje */
            System.out.println("No se puede decrementar a una velocidad negativa.");
        }
    }

    /**
     * Método que coloca la velocidad actual de un automóvil en cero
     */
    void frenar() {
        velocidadActual = 0;
    }

    /**
    * Método que calcula el tiempo que tarda un automóvil en recorrer 
    * cierta distancia
    * @param distancia Parámetro que define la distancia a recorrer por 
    * el automóvil (en kilómetros)
    */
    double calcularTiempoLlegada(int distancia) {
        return distancia/velocidadActual;
    }

    /**
    * Método que imprime en pantalla los valores de los atributos de un 
    automóvil
    */
    void imprimir() {
        System.out.println("Marca = "  + marca);
        System.out.println("Modelo = "  + modelo);
        System.out.println("Motor = "  + motor);
        System.out.println("Tipo de combustible = "  + tipoCombustible);
        System.out.println("Tipo de automóvil = "  + tipoAutomovil);
        System.out.println("Número de puertas = "  + numeroPuertas);
        System.out.println("Cantidad de asientos = " + cantidadAsientos);
        System.out.println("Velocida máxima = "  + velocidadMaxima);
        System.out.println("Color = "  + color);
    }

    /**
    * Método main que crea un automóvil, imprime sus datos en 
    * pantalla y realiza varios cambios en su velocidad
    */
    public static void main(String args[]) {
        Automovil auto1 = new Automovil("Ford",2018,3,tipoCom.DIESEL,tipoA.EJECUTIVO,5,6,250,tipoColor.NEGRO);
        auto1.imprimir();
        auto1.setVelocidadActual(100);
        System.out.println("Velocidad actual = " + auto1.velocidadActual);
        auto1.acelerar(20);
        System.out.println("Velocidad actual = " + auto1.velocidadActual);
        auto1.desacelerar(50);
        System.out.println("Velocidad actual = " + auto1.velocidadActual);
        auto1.frenar();
        System.out.println("Velocidad actual = " + auto1.velocidadActual);
        auto1.desacelerar(20);
    }
}
```

