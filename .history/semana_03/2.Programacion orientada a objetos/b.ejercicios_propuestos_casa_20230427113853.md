# Ejercicios propuestos - Casa

## Se requiere un programa que modele el concepto de un automóvil. Un automóvil tiene los siguientes atributos:

* Marca: el nombre del fabricante.
* Modelo: año de fabricación.
* Motor: volumen en litros del cilindraje del motor de un automóvil.
* Tipo de combustible: valor enumerado con los posibles valores de gasolina, bioetanol, diésel, biodiésel, gas natural.
* Tipo de automóvil: valor enumerado con los posibles valores de carro de ciudad, subcompacto, compacto, familiar, ejecutivo, SUV.
* Número de puertas: cantidad de puertas.
* Cantidad de asientos: número de asientos disponibles que tiene el vehículo.
* Velocidad máxima: velocidad máxima sostenida por el vehículo en km/h.
* Color: valor enumerado con los posibles valores de blanco, negro, rojo, naranja, amarillo, verde, azul, violeta.
* Velocidad actual: velocidad del vehículo en un momento dado.
La clase debe incluir los siguientes métodos:
* Un constructor para la clase Automóvil donde se le pasen como parámetros los valores de sus atributos.
* Métodos get y set para la clase Automóvil.
* Métodos para acelerar una cierta velocidad, desacelerar y frenar (colocar la velocidad actual en cero). Es importante tener en cuenta que no se debe acelerar más allá de la velocidad máxima permitida para el automóvil. De igual manera, tampoco es posible  desacelerar a una velocidad negativa. Si se cumplen estos casos, se debe mostrar por pantalla los mensajes correspondientes.
* Un método para calcular el tiempo estimado de llegada, utilizando como parámetro la distancia a recorrer en kilómetros. El tiempo estimado se calcula como el cociente entre la distancia a recorrer y la velocidad actual.
* Un método para mostrar los valores de los atributos de un Automóvil en pantalla.
* Un método main donde se deben crear un automóvil, colocar su velocidad actual en 100 km/h, aumentar su velocidad en 20 km/h, luego decrementar su velocidad en 50 km/h, y después frenar. Con cada cambio de velocidad, se debe mostrar en pantalla la velocidad actual

```java
/**

* Esta clase define objetos de tipo Automóvil con una marca, modelo,
* motor, tipo de combustible, tipo de automóvil, número de puertas,
* cantidad de asientos, velocidad máxima, color y velocidad actual.
* @version 1.2/2023
*/
package ejercicios;

public class Automovil {
    // Atributo que define la marca de un automóvil
    String marca;

    // Atributo que define el modelo de un automóvil
    int modelo;

    // Atributo que define el motor de un automóvil
    int motor;

    // Tipo de combustible como un valor enumerado
    enum tipoCom {GASOLINA, BIOETANOL, DIESEL, BIODISESEL, GAS_NATURAL}
    
    // Atributo que define el tipo de combustible
    tipoCom tipoCombustible;
    
    // Tipo de automóvil como un valor enumerado
    enum tipoA {CIUDAD, SUBCOMPACTO, COMPACTO, FAMILIAR, EJECUTIVO, SUV}
    
    // Atributo que define el tipo de automóvil
    tipoA tipoAutomovil;
    
    // Atributo que define el número de puertas de un automóvil
    int numeroPuertas;
    
    // Atributo que define la cantidad de asientos de un automóvil
    int cantidadAsientos;
    
    // Atributo que define la velocidad máxima de un automóvil
    int velocidadMaxima;
    
    // Color del automóvil como un valor enumerado
    enum tipoColor {BLANCO, NEGRO, ROJO, NARANJA, AMARILLO, VERDE, AZUL, VIOLETA}
    
    // Atributo que define el color de un automóvil
    tipoColor color;
    
    // Atributo que define la velocidad de un automóvil
    int velocidadActual = 0;

    /**
     * Constructor de la clase Automóvil
     * 
     * @param marca            Parámetro que define la marca de un automóvil
     * @param modelo           Parámetro que define el modelo (año de
     *                         fabricación) de un automóvil
     * @param motor            Parámetro que define el volumen del cilindraje del
     *                         motor (puede ser gasolina, bioetanol, diésel,
     *                         biodiesel o gas natural)
     * @param tipoAutomovil    Parámetro que define el tipo de automóvil
     *                         (puede ser Carro de ciudad, Subcompacto, Compacto,
     *                         Familiar,
     *                         Ejecutivo o SUV)
     * @param numeroPuertas    Parámetro que define el número de
     *                         puertas de un automóvil
     * @param cantidadAsientos Parámetro que define la cantidad de
     *                         asientos que tiene el automóvil
     * @param velocidadMaxima  Parámetro que define la velocidad
     *                         máxima permitida al automóvil
     * @param color            Parámetro que define el color del automóvil (puede
     *                         ser Blanco, Negro, Rojo, Naranja, Amarillo, Verde,
     *                         Azul o Violeta)
     */
    Automovil(String marca, int modelo, int motor, tipoCom tipoCombustible, tipoA tipoAutomovil, int numeroPuertas, int cantidadAsientos, int velocidadMaxima, tipoColor color) {
        this.marca = marca;
        this.modelo = modelo;
        this.motor = motor;
        this.tipoCombustible = tipoCombustible;
        this.tipoAutomovil = tipoAutomovil;
        this.numeroPuertas = numeroPuertas;
        this.cantidadAsientos = cantidadAsientos;
        this.velocidadMaxima = velocidadMaxima;
        this.color = color;
    }

    /**
     * Método que devuelve la marca de un automóvil
     * 
     * @return La marca de un automóvil
     */
    String getMarca() {
        return marca;
    }

    /**
    * Método que devuelve el modelo de un automóvil
    * @return El modelo de un automóvil
    */
    int getModelo() {
        return modelo;
    }

    /**
     * Método que devuelve el volumen en litros del cilindraje del motor
     * de un automóvil
     * 
     * @return El volumen en litros del cilindraje del motor de un
     *         automóvil
     */
    int getMotor() {
        return motor;
    }

    /**
     * Método que devuelve el tipo de combustible utilizado por el motor
     * de un automóvil
     * 
     * @return El tipo de combustible utilizado por el motor de un
     *         automóvil
     */
    tipoCom getTipoCombustible() {
        return tipoCombustible;
    }

    /**
     * Método que devuelve el tipo de automóvil
     * 
     * @return El tipo de automóvil
     */
    tipoA getTipoAutomovil() {
        return tipoAutomovil;
    }

    /**
     * Método que devuelve el número de puertas de un automóvil
     * 
     * @return El número de puertas que tiene un automóvil
     */
    int getNumeroPuertas() {
        return numeroPuertas;
    }

    /**
    * Método que devuelve la cantidad de asientos de un automóvil
    * @return La cantidad de asientos que tiene un automóvil
    */
    int getCantidadAsientos() {
        return cantidadAsientos;
    }
    
    /**
    * Método que devuelve la velocidad máxima de un automóvil
    * @return La velocidad máxima de un automóvil
    */
    int getVelocidadMaxima() {
        return velocidadMaxima;
    }
    
    /**
    * Método que devuelve el color de un automóvil
    * @return El color de un automóvil
    */
    tipoColor getColor() {
        return color;
    }
    
    /**
    * Método que devuelve la velocidad actual de un automóvil
    * @return La velocidad actual de un automóvil
    */
    int getVelocidadActual() {
        return velocidadActual;
    }
    
    /**
    * Método que establece la marca de un automóvil
    * @param marca Parámetro que define la marca de un automóvil 
    */
    void setMarca(String marca) {
        this.marca = marca;
    }
    
    /**
    * Método que establece el modelo de un automóvil
    * @param modelo Parámetro que define el modelo de un automóvil 
    */
    void setModelo(int modelo) {
        this.modelo = modelo;
    }
    
    /**
    * Método que establece el volumen en litros del motor de un automóvil
    * @param motor Parámetro que define el volumen en litros del 
    * motor de un automóvil 
    */
    void setMotor(int motor) {
        this.motor = motor;
    }

    /**
    * Método que establece el tipo de combustible de un automóvil
    * @param tipoCombustible Parámetro que define el tipo de 
    * combustible de un automóvil 
    */
    void setTipoCombustible(tipoCom tipoCombustible) {
        this.tipoCombustible = tipoCombustible;
    }
    
    /**
    * Método que establece el tipo de automóvil
    * @param tipoAutomovil Parámetro que define el tipo de automóvil 
    */
    void setTipoAutomovil(tipoA tipoAutomovil) {
        this.tipoAutomovil = tipoAutomovil;
    }
    
    /**
    * Método que establece el número de puertas de un automóvil
    * @param numeroPuertas Parámetro que define el número de 
    * puertas de un automóvil 
    */
    void setNumeroPuertas(int numeroPuertas) {
        this.numeroPuertas = numeroPuertas;
    }
    
    /**
    * Método que establece la cantidad de asientos de un automóvil
    * @param cantidadAsientos Parámetro que define la cantidad de 
    * asientos de un automóvil 
    */
    void setCantidadAsientos(int cantidadAsientos) {
        this.cantidadAsientos = cantidadAsientos;
    }

    /**
     * Método que establece la velocidad máxima de un automóvil
     * 
     * @param velocidadMaxima Parámetro que define la velocidad
     *                        máxima de un automóvil
     */
    void setVelocidadMaxima(int velocidadMaxima) {
        this.velocidadMaxima = velocidadMaxima;
    }

    /**
     * Método que establece el color de un automóvil
     * 
     * @param color Parámetro que define el color de un automóvil
     */
    void setColor(tipoColor color) {
        this.color = color;
    }

    /**
     * Método que establece la velocidad de un automóvil
     * 
     * @param velocidadActual Parámetro que define la velocidad actual
     *                        de un automóvil
     */
    void setVelocidadActual(int velocidadActual) {
        this.velocidadActual = velocidadActual;
    }

    /**
    * Método que incrementa la velocidad de un automóvil
    * @param incrementoVelocidad Parámetro que define la cantidad a 
    * incrementar en la velocidad actual de un automóvil 
    */
    void acelerar(int incrementoVelocidad) {
        if (velocidadActual + incrementoVelocidad < velocidadMaxima) {
            /* Si el incremento de velocidad no supera la velocidad 
            máxima */
            velocidadActual = velocidadActual + incrementoVelocidad;
        } else { /* De otra manera no se puede incrementar la velocidad y 
            se genera mensaje */
            System.out.println("No se puede incrementar a una velocidad superior a la máxima del automóvil.");
        }
    }
    
    /**
     * Método que decrementa la velocidad de un automóvil
     * 
     * @param marca Parámetro que define la cantidad a decrementar en
     *              la velocidad actual de un automovil
     */
    void desacelerar(int decrementoVelocidad) {
        /* La velocidad actual no se puede decrementar alcanzando un 
        valor negativo */
        if ((velocidadActual - decrementoVelocidad) > 0) {
            velocidadActual = velocidadActual - decrementoVelocidad;
        } else { /* De otra manera no se puede decrementar la velocidad y 
        se genera mensaje */
            System.out.println("No se puede decrementar a una velocidad negativa.");
        }
    }

    /**
     * Método que coloca la velocidad actual de un automóvil en cero
     */
    void frenar() {
        velocidadActual = 0;
    }

    /**
    * Método que calcula el tiempo que tarda un automóvil en recorrer 
    * cierta distancia
    * @param distancia Parámetro que define la distancia a recorrer por 
    * el automóvil (en kilómetros)
    */
    double calcularTiempoLlegada(int distancia) {
        return distancia/velocidadActual;
    }

    /**
    * Método que imprime en pantalla los valores de los atributos de un 
    automóvil
    */
    void imprimir() {
        System.out.println("Marca = "  + marca);
        System.out.println("Modelo = "  + modelo);
        System.out.println("Motor = "  + motor);
        System.out.println("Tipo de combustible = "  + tipoCombustible);
        System.out.println("Tipo de automóvil = "  + tipoAutomovil);
        System.out.println("Número de puertas = "  + numeroPuertas);
        System.out.println("Cantidad de asientos = " + cantidadAsientos);
        System.out.println("Velocida máxima = "  + velocidadMaxima);
        System.out.println("Color = "  + color);
    }

    /**
    * Método main que crea un automóvil, imprime sus datos en 
    * pantalla y realiza varios cambios en su velocidad
    */
    public static void main(String args[]) {
        Automovil auto1 = new Automovil("Ford",2018,3,tipoCom.DIESEL,tipoA.EJECUTIVO,5,6,250,tipoColor.NEGRO);
        auto1.imprimir();
        auto1.setVelocidadActual(100);
        System.out.println("Velocidad actual = " + auto1.velocidadActual);
        auto1.acelerar(20);
        System.out.println("Velocidad actual = " + auto1.velocidadActual);
        auto1.desacelerar(50);
        System.out.println("Velocidad actual = " + auto1.velocidadActual);
        auto1.frenar();
        System.out.println("Velocidad actual = " + auto1.velocidadActual);
        auto1.desacelerar(20);
    }
}
```

1. Agregar a la clase Automóvil, un atributo para determinar si el vehículo es automático o no. Agregar los métodos get y set para dicho atributo. Modificar el constructor para inicializar dicho atributo.
2. Modificar el método acelerar para que si la velocidad máxima se sobrepase se genere una multa. Dicha multa se puede incrementar cada vez que el vehículo intenta superar la velocidad máxima permitida.
3. Agregar un método para determinar si un vehículo tiene multas y otro método para determinar el valor total de multas de un vehículo.

## Se requiere un programa que modele varias figuras geométricas: el círculo, el rectángulo, el cuadrado y el triángulo rectángulo.

* El círculo tiene como atributo su radio en centímetros.
* El rectángulo, su base y altura en centímetros.
* El cuadrado, la longitud de sus lados en centímetros.
* El triángulo, su base y altura en centímetros.
Se requieren métodos para determinar el área y el perímetro de cada figura geométrica. Además, para el triángulo rectángulo se requiere:
* Un método que calcule la hipotenusa del rectángulo.
* Un método para determinar qué tipo de triángulo es:
  * Equilátero: todos sus lados son iguales.
  * Isósceles: tiene dos lados iguales.
  * Escaleno: todos sus lados son diferentes.
Se debe desarrollar una clase de prueba con un método main para crear las cuatro figuras y probar los métodos respectivos.

```java
package ejercicios;

/**

* Esta clase define objetos de tipo Circulo con su radio como atributo.
*
* @version 1.2/2023
 */
public class Circulo {
    int radio; // Atributo que define el radio de un círculo

    /**
  * Constructor de la clase Circulo
  *
  * @param radio Parámetro que define el radio de un círculo
     */
    Circulo(int radio) {
        this.radio = radio;
    }

    /**
  * Método que calcula y devuelve el área de un círculo como pi
  * multiplicado por el radio al cuadrado
  *
  * @return Área de un círculo
     */
    double calcularArea() {
return Math.PI* Math.pow(radio, 2);
    }

    /**
  * Método que calcula y devuelve el perímetro de un círculo como la
  * multiplicación de pi por el radio por 2
  *
  * @return Perímetro de un círculo
     */
    double calcularPerimetro() {
return 2* Math.PI * radio;
    }
}
```

## Clase Cuadrado

```java
package ejercicios;

/**

* Esta clase define objetos de tipo Cuadrado con un lado como atributo.
*
* @version 1.2/2023
 */
public class Cuadrado {
    int lado; // Atributo que define el lado de un cuadrado

    /**
  * Constructor de la clase Cuadrado
  *
  * @param lado Parámetro que define la longitud de la base de un
  * cuadrado
     */
    public Cuadrado(int lado) {
        this.lado = lado;
    }

    /**
  * Método que calcula y devuelve el área de un cuadrado como el
  * lado elevado al cuadrado
  *
  * @return Área de un Cuadrado
     */
    double calcularArea() {
return lado* lado;
    }

    /**
  * Método que calcula y devuelve el perímetro de un cuadrado como
  * cuatro veces su lado
  *
  * @return Perímetro de un cuadrado
     */
    double calcularPerimetro() {
return (4* lado);
    }
}
```

## Clase Rectangulo

```java
package ejercicios;

/**

* Esta clase define objetos de tipo Rectángulo con una base y una
* altura como atributos.
*
* @version 1.2/2023
 */
public class Rectangulo {
    int base; // Atributo que define la base de un rectángulo
    int altura; // Atributo que define la altura de un rectángulo

    /**
  * Constructor de la clase Rectangulo
  *
  * @param base   Parámetro que define la base de un rectángulo
  * @param altura Parámetro que define la altura de un rectángulo
     */
    Rectangulo(int base, int altura) {
        this.base = base;
        this.altura = altura;
    }

    /**
  * Método que calcula y devuelve el área de un rectángulo como la
  * multiplicación de la base por la altura
  *
  * @return Área de un rectángulo
     */
    double calcularArea() {
return base* altura;
    }

    /**
  * Método que calcula y devuelve el perímetro de un rectángulo
  * como (2 *base) + (2* altura)
  *
  * @return Perímetro de un rectángulo
     */
    double calcularPerimetro() {
return (2* base) + (2 * altura);
    }
}

......

package ejercicios;

/**

* Esta clase define objetos de tipo Triángulo Rectángulo con una
* base y una altura como atributos.
*
* @version 1.2/2023
 */
public class TrianguloRectangulo {
    int base; // Atributo que define la base de un triángulo rectángulo
    int altura; // Atributo que define la altura de un triángulo rectángulo

    /**
  * Constructor de la clase TriánguloRectángulo
  *
  * @param base   Parámetro que define la base de un triángulo
  * rectángulo
  * @param altura Parámetro que define la altura de un triángulo
  * rectángulo
     */
    public TrianguloRectangulo(int base, int altura) {
        this.base = base;
        this.altura = altura;
    }

    /**
  * Método que calcula y devuelve el área de un triángulo rectángulo
  * como la base multiplicada por la altura sobre 2
  *
  * @return Área de un triángulo rectángulo
     */
    double calcularArea() {
return (base* altura / 2);
    }

    /**
  * Método que calcula y devuelve el perímetro de un triángulo
  * rectángulo como la suma de la base, la altura y la hipotenusa
  * @return Perímetro de un triángulo rectángulo
    */
    double calcularPerimetro() {
        return (base + altura + calcularHipotenusa()); /* Invoca al método calcular hipotenusa */
    }

    /**
  * Método que calcula y devuelve la hipotenusa de un triángulo
  * rectángulo utilizando el teorema de Pitágoras
  *
  * @return Hipotenusa de un triángulo rectángulo
     */
    double calcularHipotenusa() {
return Math.pow(base* base + altura * altura, 0.5);
    }

    /**
  * Método que determina si un triángulo es:
  * - Equilatero: si sus tres lados son iguales
  * - Escaleno: si sus tres lados son todos diferentes
  * - Escaleno: si dos de sus lados son iguales y el otro es diferente de
  * los demás
    */
    void determinarTipoTriángulo() {
        if ((base == altura) && (base == calcularHipotenusa()) && (altura == calcularHipotenusa()))
            System.out.println("Es un triángulo equilátero"); /* Todos sus lados son iguales */
        else if ((base != altura) && (base != calcularHipotenusa()) && (altura != calcularHipotenusa()))
            System.out.println("Es un triángulo escaleno"); /* Todos sus lados son diferentes */
        else
            System.out.println("Es un triángulo isósceles"); /* De otra manera, es isósceles */
    }
}

.........

package ejercicios;

/**

* Esta clase prueba diferentes acciones realizadas en diversas figuras
* geométricas.
* @version 1.2/2023
*/
public class PruebaFiguras {
    /**
  * Método main que crea un círculo, un rectángulo, un cuadrado y
  * un triángulo rectángulo. Para cada uno de estas figuras geométricas,
  * se calcula su área y perímetro.
    */
    public static void main(String args[]) {
        Circulo figura1 = new Circulo(2);
        Rectangulo figura2 = new Rectangulo(1,2);
        Cuadrado figura3 = new Cuadrado(3);
        TrianguloRectangulo figura4 = new TrianguloRectangulo(3,5);
        System.out.println("El área del círculo es = " + figura1.
        calcularArea());
        System.out.println("El perímetro del círculo es = " + figura1.calcularPerimetro());
        System.out.println();
        System.out.println("El área del rectángulo es = " + figura2.calcularArea());
        System.out.println("El perímetro del rectángulo es = " + figura2.calcularPerimetro());
        System.out.println();
        System.out.println("El área del cuadrado es = " + figura3.calcularArea());
        System.out.println("El perímetro del cuadrado es = " + figura3.calcularPerimetro());
        System.out.println();
        System.out.println("El área del triángulo es = " + figura4.calcularArea());
        System.out.println("El perímetro del triángulo es = " + figura4.calcularPerimetro());
        figura4.determinarTipoTriángulo();
    }
}
