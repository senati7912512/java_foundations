# java_foundations

Repositorio del curso de Java Foundations.

# Programas

- [DFD](https://drive.google.com/drive/folders/1C3NE6OHlOERq9guDpLhm15a548xugf9Y?usp=share_link)

Estructura del proyecto

![Estructura de carpetas](images/estructura_curso.png)

- Presentación 01 [link](https://docs.google.com/presentation/d/1oVMk4YL0GH3cXADP9Z3nyES5FVnqOyYY)
- Presentación 02 [link](https://docs.google.com/presentation/d/1qTM9LhPjMvRWiOkLrqjvySnFyao5I6n4)
- Presentación 03 [link](https://docs.google.com/presentation/d/1r5YC8LcZgxldAGXXPuC-_UnFmKmbRXTQ)
- Presentación 04 [Link](https://docs.google.com/presentation/d/1wMgnOF1w02zPSJGb0xYEdqXyNNgJDLKH)
- Presentación 05 [Link](https://docs.google.com/presentation/d/1dH7zVGvFWnqosPgoAk5RCsCho8ReoTu8)
- Presentación 06 [Link](https://docs.google.com/presentation/d/1eqOYmcAeCc_ad-uREDhqaYZV47sJplCq)
